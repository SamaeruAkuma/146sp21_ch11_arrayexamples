import java.text.NumberFormat;

public class Product implements Comparable {

    private String code;
    private String description;
    private double price;

    public Product() {
        code = "";
        description = "";
        price = 0;
    }
    
    public Product( String code, String description, double price ) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        return currency.format(price);
    }    
    
    @Override
    public int compareTo(Object o) {
        Product p = (Product) o;
        // NOTE: we can use the usual relational operators  (<, >, ==, etc.)
        // when comparing numerical values. If you want to compare Strings,
        // you can use the compareTo method of the String class. See here
        // for more info: https://stackoverflow.com/a/4064770 
        /*
        if (this.getPrice() < p.getPrice()) {
            return -1;
        }
        if (this.getPrice() > p.getPrice()) {
            return 1;
        }
        return 0;
        */
        
        String s1 = this.getCode();
        String s2 = p.getCode();
        return s1.compareTo(s2); // this 'compareTo' is an instance method
                                 // of the String class
    } // end concrete method compareTo

}
